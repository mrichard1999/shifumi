Auteurs: Dutra Arno et Richard Martin.\
Pour utiliser Annotation.py, il suffit de regrouper vos images dans une seule vidéo.
Puis changer les chemins.
Après cela, il vous suffit de faire tourner le code et appuyer sur 'R','P','S' en fonction de la classe ou 'N' si l'image n'appartient a aucune classe.\
On note que dans les données, temps 1 correspond à ce que l'on a désigné par "Easy" et temps 2 et 3 ce que l'on a désigné par "Medium" dans le rapport.
