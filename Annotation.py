import cv2
import json
from resizeimage import resizeimage
import numpy as np
from PIL import Image


print(cv2.__version__)


cap = cv2.VideoCapture('/Users/richardmartin/Documents/test/Raw_Data/Raw_data_Temps3/Val/Test.mp4')
length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
print( length )
scanned = {}
i = 200
while cap.isOpened():
    #Read video capture
    ret, frame = cap.read()
    if ret == True:
        #Display each frame
        save_file = '/Users/richardmartin/Documents/test/Data_shifumy/Data_temps3/Val/Data/{}.png'.format(i)
        cv2.imshow("cap", frame)
        frameRGB = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = Image.fromarray(frameRGB)
        while cv2.waitKey(0) not in [ord('r'), ord('p'), ord('s'), ord('n')]:
            cv2.waitKey(0)
        # False when 'z' is pressed (no item was scanned)
        if ((cv2.waitKey(0) == ord('r'))):
            f= open("/Users/richardmartin/Documents/test/Data_shifumy/Data_temps3/Val/Labels/{}.txt".format(i),"w+")
            f.write("{}".format(0))
            f.close()
            frame = resizeimage.resize_contain(frame, [224,224],bg_color=(0, 0, 0, 0))
            frame.save(save_file)
            print(i,'r',length)
        # False when 's' is pressed (an item was scanned)
        elif ((cv2.waitKey(0) == ord('p'))):
            f= open("/Users/richardmartin/Documents/test/Data_shifumy/Data_temps3/Val/Labels/{}.txt".format(i),"w+")
            f.write("{}".format(2))
            f.close()
            frame = resizeimage.resize_contain(frame, [224,224],bg_color=(0, 0, 0, 0))
            frame.save(save_file)
            print(i,'p',length)
        elif ((cv2.waitKey(0) == ord('s'))):
            f= open("/Users/richardmartin/Documents/test/Data_shifumy/Data_temps3/Val/Labels/{}.txt".format(i),"w+")
            f.write("{}".format(1))
            f.close()
            frame = resizeimage.resize_contain(frame, [224,224],bg_color=(0, 0, 0, 0))
            frame.save(save_file)
            print(i,'s',length)
        elif ((cv2.waitKey(0) == ord('n'))):
            print(i,0)
        i = i+1
    else:
        break

cap.release
cv2.destroyAllWindows()
with open('/Users/richardmartin/Documents/data/data.json', 'w') as fp:
    json.dump(scanned, fp)